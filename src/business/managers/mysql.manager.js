'use strict';

const Connection = require('mysql/lib/Connection');
const mysql = require('mysql');

const typeUtils = require('clericuzzi-javascript/dist/utils/type.utils');
const regexUtils = require('clericuzzi-javascript/dist/utils/regex.utils');
const processUtils = require('clericuzzi-javascript/dist/utils/process.utils');

let _pool = null;

let _host = null;
let _base = null;
let _user = null;
let _pass = null;
let _port = 3306;
let _execTimeout = 5000;
let _connectionLimit = 25;
let _connectionTimeout = 10000;

const getTableNameFromError = connError =>
{
    try
    {
        const regex = /'(.[^\s]*)'/g;
        const results = regexUtils.exec(connError.sqlMessage, regex);
        for (let i = 0; i < results.length; i++)
            results[i] = results[i].replace(/'/g, ``);

        if (typeUtils.isArrayAndValid(results))
            return results[0].replace(/'/g, ``);
        else
            return connError.sqlMessage;
    }
    catch (err)
    {
        return connError.sqlMessage;
    }
};
const getConstraintNameFromError = connError =>
{
    try
    {
        const regex = /'(.[^\s]*)'/g;
        const results = regexUtils.exec(connError.sqlMessage, regex);
        for (let i = 0; i < results.length; i++)
            results[i] = results[i].replace(/'/g, ``);

        if (typeUtils.isArrayAndValid(results))
            return results[1].replace(/'/g, ``);
        else
            return connError.sqlMessage;
    }
    catch (err)
    {
        return connError.sqlMessage;
    }
};

/**
 * configures a connection without environment variables
 *
 * @param {string} host the connection's host
 * @param {string} base the connection's database
 * @param {string} user the connection's username
 * @param {string} pass the connection's password
 * @param {number} port the connection's port
 * @param {number} execTimeout the connection's exec timeout
 * @param {number} connectTimeout the connection's timeout
 * @param {number} connectionLimit the connection's connection limit
 */
module.exports.configConnection = async function (host, base, user, pass, port, execTimeout = 5000, connectTimeout = 10000, connectionLimit = 25)
{
    if (this._pool)
        this._pool.end();

    _host = host;
    _base = base;
    _user = user;
    _pass = pass;

    _port = port;
    _execTimeout = execTimeout;
    _connectionLimit = connectionLimit;
    _connectionTimeout = connectTimeout;
};

/**
 * gets a new connection from the pool
 * @param {boolean} hasTransaction TRUE if there must be a transaction, false otherwise (default)
 * @returns {Connection} a new mysqlConnection
 */
module.exports.newConnectionFromPool = async function (hasTransaction = false)
{
    const execTimeout = Number.parseInt(process.env.RDS_TIMEOUT || _execTimeout);
    const connectTimeout = Number.parseInt(process.env.RDS_CONNECTION_TIMEOUT || _connectionTimeout);
    const connection = new Promise((resolve, reject) =>
    {
        try
        {
            const poolObj = {
                port: process.env.RDS_PORT || _port,
                host: process.env.RDS_PATH || _host,
                user: process.env.RDS_USER || _user,
                password: process.env.RDS_PASS || _pass,
                database: process.env.RDS_BASE || _base,

                timeout: execTimeout,
                acquireTimeout: connectTimeout,
                connectTimeout: connectTimeout,
                connectionLimit: process.env.RDS_POOL_LIMIT || _connectionLimit
            };

            if (process.env.DEBUG)
                console.log(`creating a new connection with transaction ${hasTransaction}\n${JSON.stringify(poolObj)}`);

            if (!this._pool)
                this._pool = mysql.createPool(poolObj);

            this._pool.getConnection((connError, conn) =>
            {
                if (connError)
                {
                    if (connError.code === `ETIMEDOUT`)
                        return reject(new Error(`MySQLManager connect: the server did not respond within the limit of ${connectTimeout}ms`));
                    else
                        return reject(connError);
                }
                if (hasTransaction)
                    conn.beginTransaction();

                return resolve(conn);
            });
        }
        catch (poolErr)
        {
            return reject(poolErr);
        }
    });

    return await processUtils.race(connectTimeout, `MysqlConnection`, connection);
};
/**
 * ends the giving transaction
 * should only be called when there is no active transaction
 *
 * @param {mysql.Connection} connOrTrann the existing transactionless connection to be commited
 */
module.exports.connectionEnd = function (connOrTrann)
{
    if (!connOrTrann)
        throw new Error(`MySQLManager connectionEnd: invalid connection`);

    try
    {
        connOrTrann.release();
    }
    catch (err)
    {
        throw { message: err.message, devMessage: `MySQLManager connectionEnd: could not end the given connection` };
    }
};

const exec = async (connOrTrann, command) =>
{
    try
    {
        const execTimeout = Number.parseInt(process.env.RDS_TIMEOUT || _execTimeout);
        const isDelete = command.sql.toLocaleLowerCase().startsWith(`delete`);
        const isInsert = command.sql.toLocaleLowerCase().startsWith(`insert`);
        const isUpdate = command.sql.toLocaleLowerCase().startsWith(`update`);

        const execution = new Promise((resolve, reject) =>
        {
            let result = null;
            connOrTrann.query(command, (connError, results) =>
            {
                if (connError)
                {
                    connOrTrann.rollback();

                    if (connError.code === `ETIMEDOUT`)
                        reject(new Error(`MySQLManager exec: the server did not respond within the limit of ${execTimeout}ms`));
                    else if (connError.code === `ER_PARSE_ERROR`)
                        reject(new Error(`MySQLManager exec: syntax error -> ${connError.sqlMessage}`));
                    else if (connError.code === `ER_DUP_ENTRY`)
                        reject(new Error(`MySQLManager exec: duplicate item for table '${getTableNameFromError(connError)}', violated constraint '${getConstraintNameFromError(connError)}'`));
                    else if (connError.code === `ER_NO_SUCH_TABLE`)
                        reject(new Error(`MySQLManager exec: table '${getTableNameFromError(connError)}' does not exist`));
                    else if (connError.code === `ER_DATA_TOO_LONG`)
                        reject(new Error(`MySQLManager exec: data too long for field '${getTableNameFromError(connError)}'`));
                    else if (connError.code === `ER_BAD_FIELD_ERROR`)
                        reject(new Error(`MySQLManager exec: the field '${getTableNameFromError(connError)}' does not exist`));
                    else
                        reject(connError);
                }
                else
                {
                    if (typeUtils.isArray(results))
                        result = results.map(i => Object.assign({}, i));
                    else
                        result = Object.assign({}, results);

                    if (isDelete || isUpdate)
                        resolve(result.affectedRows);
                    if (isInsert)
                        resolve(result.insertId);
                    else
                        resolve(result);
                }
            });
        });

        return await processUtils.race(execTimeout, `MysqlQueryExecution`, execution);
    }
    catch (err)
    {
        if (process.env.DEBUG)
            console.log(`command that threw the error\n`, command);

        throw err;
    }
}

/**
 * based on the given conn we create a new request
 *
 * @param {mysql.ConnectionPool | mysql.Transaction} connOrTrann the existing, and open, connection OR transaction
 * @param {string} command the sqlCommand to be executed
 * @returns {[any]} the resulting recordset
 */
module.exports.query = async function (connOrTrann, command)
{
    if (!connOrTrann)
        throw new Error(`MySQLManager query: invalid connection`);

    const isSelect = command.sql.toLocaleLowerCase().startsWith(`select`);

    if (!isSelect)
        throw new Error(`MySQLManager query: for non select commands use the 'nonQuery' function`);

    return await exec(connOrTrann, command);
};
/**
 * based on the given conn we create a new request
 *
 * @param {mysql.ConnectionPool | mysql.Transaction} connOrTrann the existing, and open, connection OR transaction
 * @param {string} command the sqlCommand to be executed
 * @returns {number} the number of rows affected
 */
module.exports.nonQuery = async function (connOrTrann, command)
{
    if (!connOrTrann)
        throw new Error(`MySQLManager nonQuery: invalid connection`);

    const isSelect = command.sql.toLocaleLowerCase().startsWith(`select`);

    if (isSelect)
        throw new Error(`MySQLManager nonQuery: for non select commands use the 'query' function`);

    return await exec(connOrTrann, command);
};
/**
 * based on the given conn we create a new request
 *
 * @param {mysql.ConnectionPool | mysql.Transaction} connOrTrann the existing, and open, connection OR transaction
 * @param {string} command the sqlCommand to be executed
 * @returns {[any]} the resulting recordset
 */
module.exports.queryMultiple = async function (connOrTrann, command)
{
    throw new Error(`Mysql does not support this feature`);
};

/**
 * based on the given conn we create a new request
 *
 * @param {mysql.ConnectionPool | mysql.Transaction} connOrTrann the existing, and open, connection OR transaction
 * @param {string} command the sqlCommand to be executed
 * @returns {[any]} the resulting recordset
 */
module.exports.preparedQuery = async function (connOrTrann, command, values)
{
    throw new Error(`Mysql does not support this feature`);
};
/**
 * based on the given conn we create a new request
 *
 * @param {mysql.ConnectionPool | mysql.Transaction} connOrTrann the existing, and open, connection OR transaction
 * @param {string} command the sqlCommand to be executed
 * @returns {[any]} the resulting recordset
 */
module.exports.preparedNonQuery = async function (connOrTrann, command, values)
{
    throw new Error(`Mysql does not support this feature`);
};

/**
 * based on the given conn we create a new transaction
 *
 * @param {mysql.ConnectionPool} conn the existing, and open, connection
 * @returns {mysql.Transaction} the new transaction
 */
module.exports.newTransaction = async function (conn)
{
    throw new Error(`Mysql does not support this feature`);
};

/**
 * commits the given transaction
 *
 * @param {mysql.Connection} connOrTrann the existing transaction to be commited
 */
module.exports.transactionCommit = async function (connOrTrann)
{
    if (!connOrTrann)
        throw new Error(`MySQLManager transactionCommit: invalid connection`);

    try
    {
        await connOrTrann.commit();
    }
    catch (err)
    {
        throw { message: err.message, devMessage: `MySQLManager transactionCommit: could not commit the given transaction` };
    }
    finally
    {
        connOrTrann.release();
    }
};
/**
 * rolls back the given transaction
 *
 * @param {mysql.Connection} connOrTrann the existing transaction to be rolled back
 */
module.exports.transactionRollback = async function (connOrTrann)
{
    if (!connOrTrann)
        throw new Error(`MySQLManager transactionRollback: invalid connection`);

    try
    {
        await connOrTrann.rollback();
    }
    catch (err)
    {
        throw { message: err.message, devMessage: `MySQLManager transactionRollback: could not roll back the given transaction` };
    }
    finally
    {
        connOrTrann.release();
    }
};

/**
* default listing method based in a SqlBuilder { id: x, label: y }
*
* @param {mysql.ConnectionPool | mssql.Transaction} connOrTrann the existing, and open, connection OR transaction
* @param {string} table the table to get data from
* @param {string} field the field to be fetched
* @param {string} id the table's id field
* @returns a list of <id, label> pairs
*/
module.exports.list = async function (connOrTrann, table, field, id = `id`)
{
    if (!connOrTrann)
        throw new Error(`MySQLManager list: invalid connection`);
    if (!field)
        throw new Error(`MySQLManager list: invalid payload, field name must be provided`);
    if (!table)
        throw new Error(`MySQLManager list: invalid payload, a table name must be provided`);

    const command = { sql: `select \`${id}\` as \`id\`, \`${field}\` as \`label\` from \`${table}\`` };
    const result = this.query(connOrTrann, command);

    return result;
};
/**
 * counts the rows in tge given table
 *
 * @param {mysql.ConnectionPool | mssql.Transaction} connOrTrann the existing, and open, connection OR transaction
 * @param {string} table the table to have it's rows counted
 * @returns the number of rows
 */
module.exports.count = async function (connOrTrann, table)
{
    if (!connOrTrann)
        throw new Error(`MySQLManager count: invalid connection`);
    if (!table)
        throw new Error(`MySQLManager count: invalid payload, a table name must be provided`);

    const command = { sql: `select count(0) as \`counter\` from \`${table}\`` };
    const result = await this.query(connOrTrann, command);
    if (typeUtils.isArrayAndValid(result))
        return result[0].counter;
    else
        return result;
};