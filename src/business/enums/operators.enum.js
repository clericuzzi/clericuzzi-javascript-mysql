module.exports = {
    in: `in`,
    like: `like`,
    equals: `=`,
    between: `between`,
    lessThan: `<`,
    greaterThan: `>`,
    lessThanEquals: `<=`,
    greaterThanEquals: `>=`,

    notIn: `not in`,
    notLike: `not like`,
    notEquals: `<>`,
};