const operators = require('../business/enums/operators.enum');
const typeUtils = require('clericuzzi-javascript/dist/utils/type.utils');
const arrayUtils = require('clericuzzi-javascript/dist/utils/array.utils');

/**
 * formats the LIMIT command that will be executed
 *
 * @param {{pageSize: number, page: number}} limitInfo the range of values that will be returned
 * @returns {string} the LIMIT command
 */
module.exports.getLimit = limitInfo =>
{
    if (!limitInfo)
        return ``;
    else
    {
        const { page, pageSize } = limitInfo;
        if (page === null || page === undefined)
            throw new Error(`MysqlUtils getLimit: invalid payload, PAGE cannot be null`);
        if (page <= 0)
            throw new Error(`MysqlUtils getLimit: invalid payload, PAGE cannot be zero or negative ('${page}')`);
        if (pageSize === null || pageSize === undefined)
            throw new Error(`MysqlUtils getLimit: invalid payload, PAGESIZE cannot be null`);
        if (pageSize < 1)
            throw new Error(`MysqlUtils getLimit: invalid payload, PAGESIZE cannot be less than 1 ('${pageSize}')`);

        return `limit ${(page - 1) * pageSize}, ${pageSize}`;
    }
};

/**
 * formats the ORDER BY command that will be executed
 *
 * @param {[{column: string, direction: ('asc'|'desc')}]} sortingInfo the columns and sorting direction to order the collection by
 * @param {boolean} [orderBy=true] if true, the reserverd word ORDER BY will be included in the returned command
 * @returns {string} the ORDER BY command
 */
module.exports.getSorting = (sortingInfo, orderBy = true) =>
{
    if (!sortingInfo)
        return ``;
    else
        return `${orderBy ? `order by` : ``} ${sortingInfo.map(i => getSort(i)).join(`, `)}`.trim();
};
const getSort = sortingItem =>
{
    const { column, direction } = sortingItem;
    if (!column)
        throw new Error(`MysqlUtils getSorting: invalid sorting configuration ('${JSON.stringify(sortingItem)}')`);
    if (!typeUtils.isStringAndValid(direction))
        direction = `asc`;

    return `\`${column}\` ${direction}`;
};

/**
 * formats the WHERE command that will be executed
 *
 * @param {[{column: string, operator: operators, value: any, valueMax: number}]} filters the commands that will filter the result set
 * @param {boolean} [where=true] if true, the reserverd word WHERE will be included in the returned command
 * @returns {string} the WHERE command
 */
module.exports.getFilters = (filters, where = true) =>
{
    if (!filters)
        return ``;
    else
    {
        return `${where ? `where` : ``} ${filters.map(i => getFilter(i)).join(`, `)}`.trim();
    }
};
/**
 * formats a single filter
 *
 * @param {{column: string, operator: operators, value: any, valueMax: number}} filter the filter configuration
 * @returns {string} the filter command
 */
const getFilter = filter =>
{
    const { column, operator } = filter;
    if (!column || !operator)
        throw new Error(`MysqlUtils getFilters: invalid filter configuration ('${JSON.stringify(filter)}')`);

    switch (operator)
    {
        case operators.like: return `\`${column}\` ${operator} '%?%'`;
        case operators.notLike: return `\`${column}\` ${operator} '%?%'`;
        case operators.between: return `\`${column}\` ${operator} ? and ?`;

        default: return `\`${column}\` ${operator} ?`;
    }
};

/**
 * formats the WHERE command that will be executed
 *
 * @param {[{column: string, operator: operators, value: any, valueMax: number}]} filters the commands that will filter the result set
 * @returns {[*]} the values collection involved in the filter operation
 */
module.exports.getFiltersValues = (filters) =>
{
    if (!filters)
        return [];
    else
        return arrayUtils.flatten(filters.map(i => getFilterValue(i)));
};
/**
 * gets the value of a single filter
 *
 * @param {{column: string, operator: operators, value: any, valueMax: number}} filter the filter configuration
 * @returns {[*]} the filter's value
 */
const getFilterValue = filter =>
{
    const { operator, value, valueMax } = filter;
    if (operator === operators.between && !typeUtils.isValid(valueMax))
        throw new Error(`MysqlUtils getFiltersValues: invalid filter configuration ('${JSON.stringify(filter)}')`);

    switch (operator)
    {
        case operators.between: return [value, valueMax];

        default: return [value];
    }
};