const typeUtils = require('clericuzzi-javascript/dist/utils/type.utils');
const mysqlManager = require('../business/managers/mysql.manager');

const CrudBase = require('clericuzzi-javascript-crud/dist/models/crud.base');
const databaseTypes = require('clericuzzi-javascript-crud/dist/business/enums/databaseTypes.enum');

module.exports = class MysqlCrudModel extends CrudBase
{
    /**
     * Creates an instance of MssqlCrudModel.
     * @param {string} tableName the represented table's name
     * @param {[string]} columns the columns list
     * @param {[string]} dataTypes the data types list
     * @param {[string]} nullableColumns the nullable columns list
     * @param {[string]} fkColumns thr fks list
     * @param {boolean} hasRemoved if TRUE the table has the 'removed' field
     * @param {string} idProperty the id property's name
     * @param {any} schema the protobuf schema
     */
    constructor(tableName, columns, dataTypes, nullableColumns, fkColumns, hasRemoved, idProperty, schema)
    {
        super(tableName, columns, dataTypes, nullableColumns, fkColumns, hasRemoved, idProperty, schema, databaseTypes.MySQL);
    }

    async count(conn)
    {
        this.validate();

        return mysqlManager.count(this.tableName, conn);
    }

    async load(conn)
    {
        if (!conn)
            throw new Error(`CrudModel load: connection required`);
        if (!this[this.idProperty])
            throw { code: 0, devMessage: `CrudModel load: cannot LOAD a '${this.tableName}' with an invalid id`, humanMessage: `Não é possível carregar um item sem ID` };

        this.validate();

        const queryObj = { sql: `select * from \`${this.tableName}\` where \`${this.idProperty}\` = ?`, values: [this[this.idProperty]] };
        const result = await mysqlManager.query(conn, queryObj);
        if (typeUtils.isArrayAndValid(result))
            for (let column of this.columns)
                this.loadProperty(column, result[0][column]);
        else
            throw { code: 0, devMessage: `CrudModel load: the id '${this[this.idProperty]}' does not exist in table '${this.tableName}'`, humanMessage: `O item solicitado não existe` };
    }
    async loadAll(conn)
    {
        if (!conn)
            throw new Error(`CrudModel loadAll: connection required`);

        await this.load(conn);
        for (let fkColumn of this.fkColumns)
        {
            const fkProperty = fkColumn.slice(0, -2);
            if (this[fkProperty] && this[fkProperty].id && this[fkProperty].loadAll)
                await this[fkProperty].loadAll(conn);
        }
    }

    async insert(conn)
    {
        if (!conn)
            throw new Error(`CrudModel insert: connection required`);
        this.validate();

        const values = [];
        const questionMarks = [];
        for (let column of this.columns)
        {
            questionMarks.push(`?`);
            values.push(typeUtils.isValid(this[column]) ? this[column] : null);
        }

        const queryObj = { sql: `insert into \`${this.tableName}\` values (${questionMarks})`, values };
        const result = await mysqlManager.nonQuery(conn, queryObj);
        if (result && result > 0)
            this[this.idProperty] = result;
        else
            throw { code: 0, devMessage: `CrudModel insert: '${this.tableName}' unknown error`, humanMessage: `Erro desconhecido no cadastro do item` };
    }
    async update(conn, ...properties)
    {
        if (!conn)
            throw new Error(`CrudModel update: connection required`);
        if (!this[this.idProperty])
            throw { code: 0, devMessage: `CrudModel update: cannot UPDATE a '${this.tableName}' with an invalid id`, humanMessage: `Não é possível alterar um objeto com ID inválido` };
        if (!typeUtils.isArrayAndValid(properties))
            throw { code: 0, devMessage: `CrudModel update: cannot UPDATE a '${this.tableName}' without the target properties`, humanMessage: `Não é possível alterar um objeto sem propriedades` };

        this.validate();

        const values = [];
        const questionMarks = [];
        for (let property of properties)
        {
            this.validateProperty(property);
            questionMarks.push(`\`${property}\` = ?`);
            values.push(this[property]);
        }
        values.push(this[this.idProperty]);
        const queryObj = { sql: `update \`${this.tableName}\` set ${questionMarks.join(`,`)} where \`${this.idProperty}\` = ?`, values };

        const result = await mysqlManager.nonQuery(conn, queryObj);
        if (!result)
            throw { code: 0, devMessage: `CrudModel update: '${this.tableName}' unknown error`, humanMessage: `Erro desconhecido na alteração do item` };
        else
            return result > 0;
    }
    async delete(conn)
    {
        if (!conn)
            throw new Error(`CrudModel delete: connection required`);
        if (!this[this.idProperty])
            throw { code: 0, devMessage: `CrudModel delete: cannot DELETE a '${this.tableName}' with an invalid id`, humanMessage: `Impossível remover um item sem id` };

        this.validate();
        let queryObj = ``;
        if (this.hasRemoved)
        {
            this[`removed`] = 1;
            queryObj = { sql: `update \`${this.tableName}\` set \`removed\` = 1 where \`${this.idProperty}\` = ?`, values: [this[this.idProperty]] };
        }
        else
            queryObj = { sql: `delete from \`${this.tableName}\` where \`${this.idProperty}\` = ?`, values: [this[this.idProperty]] };

        const result = await mysqlManager.nonQuery(conn, queryObj);
        if (!result)
            throw { code: 0, devMessage: `CrudModel delete: '${this.tableName}' unknown error`, humanMessage: `Erro desconhecido na remoção do item` };
        else
            return result > 0;
    }
    async undelete(conn)
    {
        if (!conn)
            throw new Error(`CrudModel undelete: connection required`);
        if (!this.hasRemoved)
            throw { code: 0, devMessage: `CrudModel undelete: cannot UNDELETE an item from '${this.tableName}'. There is no 'removed' field`, humanMessage: `Impossível restaurar um item sem a coluna 'removido'` };
        if (!this[this.idProperty])
            throw { code: 0, devMessage: `CrudModel undelete: cannot UNDELETE a '${this.tableName}' with an invalid id`, humanMessage: `Impossível restaurar um item sem id` };

        this[`removed`] = 0;
        this.validate();
        let queryObj = { sql: `update \`${this.tableName}\` set \`removed\` = 0 where \`${this.idProperty}\` = ?`, values: [this[this.idProperty]] };

        const result = await mysqlManager.nonQuery(conn, queryObj);
        if (!result)
            throw { code: 0, devMessage: `CrudModel delete: '${this.tableName}' unknown error`, humanMessage: `Erro desconhecido na remoção do item` };
        else
            return result > 0;
    }

    toJson() { return super.toJson(); }
    toBinary() { return super.toBinary(); }
    toSchema() { return super.toSchema(); }

    fromJson(json) { return super.fromJson(json); }
    fromSchema(pb) { return super.fromSchema(pb); }
    fromBinary(bytes) { return super.fromBinary(bytes); }
}