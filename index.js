const mysqlManager = require('./src/business/managers/mysql.manager');
mysqlManager.configConnection(`localhost`, `concretio_dev`, `root`, `12qwaszx`, 3306, 60000, 30000);

const codeGenerator = require('./src/business/managers/codeGeneration.manager');
const Event = require('./src/tst/models/concretio_dev/evt_event.model');

(async function init()
{
    try
    {
        // await codeGenerator.generate(`C:\\clericuzzi-projects\\clericuzzi-javascript-mysql\\src\\tst`, `concretio_dev`)

        const conn = await mysqlManager.newConnectionFromPool();

        // const evt = new Event(1);
        // await evt.load(conn);
        // console.log(evt);
        // console.log(`LOAD DONE`);

        // const evt = new Event(null, `tst`);
        // await evt.insert(conn);
        // console.log(`insert DONE`);

        // const evt = new Event(19);
        // await evt.load(conn);
        // evt.evt_event = `OLAR!`;
        // await evt.update(conn, evt.columns[1]);
        // console.log(`UPDATE DONE`);

        // const evt = new Event(19);
        // await evt.delete(conn);
        // console.log(`DELETE DONE`);

        // const evt = new Event(20);
        // await evt.undelete(conn);
        // console.log(`UNDELETE DONE`);
    }
    catch (err)
    {
        console.log(`err:`, err);
        return;
    }
})();